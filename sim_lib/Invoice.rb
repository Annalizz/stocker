require_relative 'VendorFactory'

class Invoice
  INVOICE_NAME_PREFIXES = ["PI", "SI", "IR", "TN", "CR", "WP", "BS", "AF"] #constant with possible invoice name prefixes. 1 is chosen at random on creation
  
  def initialize
    @invoice_num = " #{ INVOICE_NAME_PREFIXES[rand(INVOICE_NAME_PREFIXES.length)] }-#{ rand(900) + 100 }" 
    @vendor = VendorFactory.instance.get_vendor
    @current_line = 0
    @invoice_date = Time.at(0.0 + rand * (Time.now.to_f - 0.0.to_f))
  end
  
  def print_new_line
    @current_line += 1
  end
  
  def print_invoice_num
    @invoice_num
  end
  
  def print_vendor_name
    @vendor.print_name
  end
  
  def print_invoice_date
    @invoice_date.strftime("%Y/%m/%d")
  end
end