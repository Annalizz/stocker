require 'singleton'
require_relative 'Vendor'

class VendorFactory
  include Singleton #there can be only 1 vendor factory.... inkella nispiccaw kapitalizmu sfrenat rofl
  
  def initialize
    _names_urls = "http://uinames.com/api/?amount=50&region=England"
    _resp = Net::HTTP.get_response(URI.parse(_names_urls))
    _json_names_response = JSON.parse(_resp.body)
    _vendor_suffixes = ["& Sons", "& Daughters", "& Genderless Children", "& Penguins", "plc", "GmbH", "Ltd", "Independent Traders"]
    
    @vendor_list = [] #array to store list of pre-generated vendors
    
    #generate 10 random vendors and store them in an array. They will the be used randomly by invoices.
    for _i in 1..10
      _vendor_name = "#{ _json_names_response[_i]['name'] } #{ _json_names_response[_i]["surname"] } #{ _vendor_suffixes[rand(8)] }"
      @vendor_list.push(Vendor.new(_vendor_name))
    end
  end
  
  def get_vendor
    #get a random vendor from the pre-generated list
    _index = rand(10)
    
    @vendor_list[_index]
  end
end