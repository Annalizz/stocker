class Product
  def initialize(name,qty,price)
    @name = name
    @qty = qty
    @price = price
  end
  
  def print_name
    @name
  end
  
  def print_qty
    @qty
  end
  
  def print_price
    @price
  end
  
  def print_total_value
    (@price * @qty).round(2)
  end
end