class Comment
  def initialize(author,comment_text)
    @comment_text = comment_text
    @author = author
  end
  
  def print_text
    @comment_text
  end
  
  def print_author
    @author
  end
  
  def print_comment
    "#{@comment_text} - #{@author}"
  end
end