class Vendor
  def initialize(vendor_name)
    @vendor_name = vendor_name
  end
  
  def print_name
    @vendor_name
  end
end