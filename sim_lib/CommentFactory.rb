require 'singleton'

require_relative 'Comment'

class CommentFactory
  include Singleton
  
  def initialize
    @comment_list = []
    
    _quote_url = 'http://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=40'
    _resp = Net::HTTP.get_response(URI.parse(_quote_url))
    _json_response = JSON.parse(_resp.body)
    
    for _i in 0.._json_response.length - 1
      _content = _json_response[_i]['content']
    
      # clean content from newline, paragraphs u hekk
      _content = _content.to_s.gsub! "\n", ""
      _content = _content.to_s.gsub! '<p>', ''
      _content = _content.to_s.gsub! '</p>', ''
      
      _author = _json_response[_i]['title']
      
      @comment_list.push(Comment.new(_author, _content))
    end 
  end
  
  def get_comment
    _index = rand(@comment_list.length)
    
    @comment_list[_index]
  end
end
