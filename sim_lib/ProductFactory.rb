require 'singleton'
require_relative 'Product'

class ProductFactory
  include Singleton
  
  def initialize
    @products = ["pencils", "pens", "rubbers", "mouse", "keyboards", "monitors", "mobiles", "tea", "coffee", "penguins", "fish"]
  end
  
  def expand_products
    @products.push("turtles", "elephants")
  end
  
  def get_product(single_qty)
    _chosen_product = @products[rand(@products.length)]
    _qty = single_qty ? 1 : 1+rand(50)
    _price = (rand * 200).round(2)
    
    Product.new(_chosen_product, _qty, _price)
  end
end