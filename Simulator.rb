require 'net/http'
require 'json'

require_relative "sim_lib/Invoice"
require_relative "sim_lib/CommentFactory"
require_relative "sim_lib/ProductFactory"

def write_comment(file_obj)
  if 1+rand(10) == 9
    _comment = CommentFactory.instance.get_comment
    
    file_obj.syswrite("# #{ _comment.print_comment } \n")
  end
end

def append_string(invalid_file, invalid_rows)
  if invalid_file || (invalid_rows && 1 + rand(20) == 9)
    ", invalid"
  else
    ""
  end
end

option_param = ARGV[0].to_s.strip

if option_param == '--help' || option_param == '-h'
  puts "Usage: ruby Simulator.rb [option]"
  puts "Simulator for Stocker CSV file. Usage: "
  puts "\nOptions:"
  puts "   [blank]:        Specifying no parameter witll generate a valid CSV file as per spec"
  puts "   --valid:        A valid CSV file as per blank"
  puts "   --invalid_rows: A CSV file with mostly valid lines, but a few invalid ones...roughly 1 in 20 will be invalid"
  puts "   --invalid_file: A CSV file with all invalid lines."
  puts "   --single_qty:   A valid CSV file where quantities are always singular" 
  exit
end

invalid_rows = false
invalid_file = false
single_qty = false
CSV_DELIMITER = ';'

case option_param
when "--invalid_rows"
  invalid_rows = true
  puts "Invalid Rows chosen, some rows will be intentionally invalid"
when "--invalid_file"
  invalid_file = true
  puts "Invalid file chosen, file will all be invalid"
when "--single_qty"
  single_qty = true
  puts "All quantities will be single"
end

puts "Creating Products Response file"
csv_file = File.new("products.csv", "w")

prod_factory = ProductFactory.instance

puts "Filling in File"
for i in 0..1000
  product = prod_factory.get_product(single_qty)
  
  csv_file.syswrite("#{ product.print_name } #{ CSV_DELIMITER } #{ product.print_qty } #{ CSV_DELIMITER }  #{ product.print_price } #{ append_string(invalid_file, invalid_rows) }\n")
  
  write_comment(csv_file)
end

csv_file.close
puts "Finished creating products file"

puts "Preparing and creating Invoices File"
invoice_file = File.new("invoices.csv", "w")

invoices_amounts = rand(50) + 20 #We need at least 20. Any less and I don't belive it will be really relevant
invoices = []

for i in 0..invoices_amounts
  invoices.push(Invoice.new)
end

prod_factory.expand_products

puts "Finished preparing, populating file"

for i in 0..200 
  invoice = invoices[rand(invoices.length)]
  
  product = prod_factory.get_product(single_qty)
  
  invoice_file.syswrite("#{ invoice.print_invoice_date } #{ CSV_DELIMITER }   #{ invoice.print_new_line } #{ CSV_DELIMITER }  #{ invoice.print_invoice_num } #{ CSV_DELIMITER }  "\
                         "#{ product.print_name } #{ CSV_DELIMITER }  #{ invoice.print_vendor_name } #{ CSV_DELIMITER }  #{ product.print_price } #{ CSV_DELIMITER }  #{ product.print_qty } #{ CSV_DELIMITER }  "\
                         "#{ product.print_total_value } #{ append_string(invalid_file, invalid_rows) }\n")
  
  write_comment(invoice_file)
end

invoice_file.close
puts "Finished creating invoice file"
