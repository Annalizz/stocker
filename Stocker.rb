require_relative "lib/CSVParser"
require_relative "lib/Outputter"
require_relative "lib/Stock"

#include Stock

file_location = ARGV[0] ? ARGV[0].strip : "products.csv" 

begin
  csv_parser = CSVParser.new
  stock = Stock.instance

  csv_parser.parse_products_file(file_location, stock)
  
  outputter = Outputter.instance

  outputter.print_stock(stock.products_loaded)
rescue Exception => e
   puts e.message
end