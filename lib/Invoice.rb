class Invoice

	def initialize(lines, number, total, purchase_date)
		@lines = lines
		@number = number
		@total = total
		@purchase_date = purchase_date
	end

	def print_invoice_lines
		@invoice_lines
	end

	def print_number
		@number
	end

	def print_total
		@total
	end

	def print_purchase_date
		@purchase_date
	end
end