class Product 
	
	def initialize(name, qty, value)
		validate(name, qty, value)

		@name = name
		@qty = qty
		@value = value
	end

	def name
		@name
	end

	def qty
		@qty		
	end

	def value
		@value
	end

	def add_qty (qty)
		@qty += qty
	end

	def add_value (value)
		@value += value
	end

	def is_equal (other_product)
		name.eql?other_product.name
	end

	private def validate(name, qty, value)
		if name.to_s.empty?
			raise "Invalid Product Name!"
		end

		validate_integer(qty, "Quantity")
		validate_integer(value, "Value")	
	end

	private def validate_integer(int, field_name)
		if !int.is_a? Integer || int.to_i < 0
			raise "Invalid " + field_name + " specified!"
		end
	end
end