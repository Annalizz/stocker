require 'securerandom'

class Vendor

	def initialize(name)
		@name = name
		@id = SecureRandom.uuid
	end

	def print_name
		@name
	end

	def print_id
		@id
	end
end