require 'singleton'

class Outputter
	include Singleton

	LINE_SEPARATOR = "+====================================================+===========================+"
	NAME_MAX_LENGTH = 50;
	QTY_MAX_LENGTH = 25;

	def print_stock(stock)
		puts LINE_SEPARATOR
		puts "|          Product Name                              |      Available Qty        |"
		puts LINE_SEPARATOR
		stock.each do |name, product|
			print_product_line(name, product)
			puts LINE_SEPARATOR
		end
	end

	private 
	def print_product_line(name, product)
		get_name_arr(name).each_with_index do |name_part, index|
			print "| " + format_string(name_part, NAME_MAX_LENGTH) + " | "
			print index == 0 ? format_string(product.qty.to_s + " " + product.value.to_s, QTY_MAX_LENGTH) : format_string("", QTY_MAX_LENGTH)
			puts " |"
		end
	end

	def get_name_arr(name) 
		name.scan(/.{1,#{NAME_MAX_LENGTH}}/)
	end

	def format_string(str, max_len)
		str.length == max_len ? str : str + fill_space(max_len - str.length)
	end

	def fill_space(length)
		" " * length
	end
end