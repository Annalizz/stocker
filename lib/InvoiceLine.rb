class InvoiceLine

	def initialize(number, product, vendor, unit_price, qty)
		@number = number
		@product = product
		@vendor = vendor
		@unit_price = unit_price
		@qty = qty
	end
	
	def print_number
		@number
	end

	def print_product
		@product
	end

	def print_vendor
		@vendor
	end

	def print_unit_price
		@unit_price
	end

	def print_qty
		@qty
	end
end