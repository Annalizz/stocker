require 'singleton'
require_relative 'Product'

class Stock
	include Singleton

	def initialize
		@products_loaded = Hash.new
	end

	def add_item (product_name, quantity, value)
		_product_loaded = @products_loaded[product_name]

		# If product is not loaded, create new product. Otherwise, add quantity and value to the existing product
		if _product_loaded.nil?
			@products_loaded[product_name] = Product.new(product_name, quantity, value)
		else
			_product_loaded.add_qty(quantity)
			_product_loaded.add_value(value)
		end
	end

	def products_loaded
		@products_loaded
	end
end