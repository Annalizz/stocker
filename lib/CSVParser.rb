require "date"

class CSVParser

	COMMENT_CHAR = "#"
	CSV_SEPARATOR = ";"
	TIME_FORMAT = '%d/%m/%Y %H:%M.%L'

	def parse_products_file(filepath, stock)
		$log_file = File.new("stocker.log", "w")
		
		begin  
			$log_file.syswrite("Initialising STOCKER log file\n")
		
			if !File.exist?(filepath)
				$log_file.syswrite("#{Time.now.strftime(TIME_FORMAT)} ERROR: File #{filepath} does not exist")
					
				# throw exception since file cannot be parsed
				raise "File #{filepath} does not exist"
			end
		  
			$comments_skipped = 0
			$invalid_entries = 0
	  
			IO.foreach(filepath){|stock_line| parse_products_line(stock_line, stock)}
			$log_file.syswrite("Finished parsing CSV. Summary:\nSkipped #{$invalid_entries} invalid entries and #{$comments_skipped} comments")
			
			ensure
				$log_file.close
		end
	end

	private 
	def parse_products_line(line, stock)
		if line.strip[0, 1] == COMMENT_CHAR 
			# skip any comments
			$comments_skipped += 1
			$log_file.syswrite("#{Time.now.strftime(TIME_FORMAT)} INFO: Found comment on line #{$.}: #{line}")
		elsif line !~ /^.+;\s*[0-9]+\s*;\s*[0-9]+(\.[0-9]{1,2})?\s*$/
			# skip any invalid entries
			$invalid_entries += 1
			$log_file.syswrite("#{Time.now.strftime(TIME_FORMAT)} WARN: Found invalid entry on line #{$.}: #{line}")
		else
			# valid entry - parse the name, quantity and value, and add item to the stock
			line_arr = line.split(CSV_SEPARATOR)
			_name = line_arr[0].strip
			_qty = line_arr[1].strip
			_value = line_arr[2].strip
			stock.add_item(_name, _qty.to_i, _value.to_i)
		end
	end
end